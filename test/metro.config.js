
/**
 * Metro Bundler configuration
 * https://facebook.github.io/metro/docs/en/configuration
 *
 * eslint-env node, es6
 */

 const getWorkspaces = require('get-yarn-workspaces');
 const path = require('path');
 
 function getConfig(appDir, options = {}) {
   const workspaces = getWorkspaces(appDir);
 
   // Add additional Yarn workspace package roots to the module map
   // https://bit.ly/2LHHTP0
   const watchFolders = [
     path.resolve(appDir, '..', 'node_modules'),
     ...workspaces.filter(
       workspaceDir => !(workspaceDir === appDir),
     ),
   ];
 
   return {
     watchFolders,
     resolver: {
 
       extraNodeModules: {
         // Resolve all react-native module imports to the locally-installed version
         'react-native': path.resolve(appDir, 'node_modules', 'react-native'),
 
         // Resolve additional nohoist modules depended on by other packages
         'react-native-svg': path.resolve(
           appDir,
           'node_modules',
           'react-native-svg',
         ),
 
         // Resolve core-js imports to the locally installed version
         'core-js': path.resolve(appDir, 'node_modules', 'core-js'),
       },
     },
   };
 }
 
 module.exports = getConfig(__dirname);
 
 // NEW ATTEMPT
 // /** //  * Metro configuration for React Native //  * https://github.com/facebook/react-native //  * //  * @format //  */
 // const path = require('path')
 // module.exports = {
 //   transformer: {
 //     getTransformOptions: async () => ({
 //       transform: {
 //         experimentalImportSupport: false,
 //         inlineRequires: false,
 //       },
 //     }),
 //   },
 //   watchFolders: [path.resolve(__dirname, '../node_modules')],
 // }; 